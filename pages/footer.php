<footer class="footer">
        <div class="row">
            <div class="footer__infos">
                <img class="logo" src="./img/logo-footer.png" alt="Logo Leo Learning">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
            </div>
            <div class="footer__contato">
                <h3>// CONTATO</h3>
                <p>(21) 98765-4321</p>
                <p>contato@leolearning.com</p>
            </div>
            <div class="footer__redes-sociais">
                <h3>// REDES SOCIAIS</h3>
                <div class="footer__icones">
                    <a href="">
                        <i class="fab fa-twitter"></i>
                    </a>
                    <a href="">
                        <i class="fab fa-youtube"></i>
                    </a>
                    <a href="">
                        <i class="fab fa-pinterest"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="copyright">
            <p>Copyright 2018 - All right reserved.</p>
        </div>

    </footer>

    <!-- Modal -->
    <div class="background-modal">
        <div class="modal">
            <a href="#" onclick="fecharModal()" class="btn-fechar">
                <span class="close">x</span>
            </a>
            <div class="img-modal">
                <img src="./img/modal.png" alt="">
            </div>
            <div class="modal-descricao">
                <h2>EGESTAS TORTOR VULPUTATE</h2>
                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Non, perferendis? Quaerat impedit id suscipit animi esse harum saepe! Quo error iure ratione</p>
                <form action="subscribe/addUser.php" method="post">
                <div class="form-group">
                    <input type="text" id="first_name" name="first_name" placeholder="Digite seu nome">
                </div>    
                <div class="form-group">
                    <input type="text" id="last_name" name="last_name" placeholder="Digite seu sobrenome">
                </div> 
                <div class="form-group">
                    <input type="text" id="email" name="email" placeholder="Digite seu email">
                </div>
                <a onclick="subscribe(this)" href="">INSCREVA-SE</a>
                </form>
            </div>
        </div>
    </div>
    <!-- Fim Modal-->

    <script src="./js/app.js"></script>
</body>

</html>