<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="./css/normalize.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
        crossorigin="anonymous">
    <link rel="stylesheet" href="./css/style.css">

    <title>Desafio Leo</title>
</head>

<body>

    <header>
        <nav class="menu">
            <a href="">
                <img class="logo" src="./img/logo.png" alt="Logo Leo Learning">
            </a>
            <div class="menu__bloco-direita">
                <div class="menu__pesquisa">
                    <input type="text" placeholder="Pesquisar cursos...">
                    <a href="">
                        <i class="fas fa-search"></i>
                    </a>
                </div>
                <span class="pipe"></span>
                <div class="menu__info-usuario">
                    <img src="./img/perfil.png" alt="Perfil do Usuário">
                    <div class="info-usuario__bloco-nome">
                        <span>Seja bem-vindo</span>
                        <span id="nome-visitante">Jhon</span>
                    </div>
                    <!-- <i class="fas fa-caret-down"></i> -->
                </div>
            </div>
        </nav>
    </header>