<div class="carousel">

        <div class="carousel__slides fade">
            <img class="carousel__imagem" src="./img/carousel-2.jpg" alt="Imagem carousel">
            <div class="carousel__bloco-mensagem right">
                <h2>LOREM IPSUM</h2>
                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Fuga ratione, sequi placeat, omnis corrupti modi
                    aut est quis possimus magni aspernatur reprehenderit iusto assumenda eaque dolorum dolore voluptatum,
                    quas consequuntur!</p>
                <a href="#">VER CURSO</a>
            </div>
        </div>
        <div class="carousel__slides fade">
                <img class="carousel__imagem" src="./img/carousel-1.jpg" alt="Imagem carousel">
                <div class="carousel__bloco-mensagem">
                    <h2>LOREM IPSUM</h2>
                    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Fuga ratione, sequi placeat, omnis corrupti modi
                        aut est quis possimus magni aspernatur reprehenderit iusto assumenda eaque dolorum dolore voluptatum,
                        quas consequuntur!</p>
                    <a href="#">VER CURSO</a>
                </div>
            </div>
        <div class="carousel__slides fade">
            <img class="carousel__imagem" src="./img/curso.jpg" alt="Imagem carousel">
            <div class="carousel__bloco-mensagem">
                <h2>LOREM IPSUM</h2>
                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Fuga ratione, sequi placeat, omnis corrupti modi
                    aut est quis possimus magni aspernatur reprehenderit iusto assumenda eaque dolorum dolore voluptatum,
                    quas consequuntur!</p>
                <a href="#">VER CURSO</a>
            </div>
        </div>

        <a class="carousel__prev" onclick="plusSlides(-1)">
            <i class="fas fa-angle-left"></i>
        </a>
        <a class="carousel__next" onclick="plusSlides(1)">
            <i class="fas fa-angle-right"></i>
        </a>

        <div class="carousel__dots">
                <span class="dot" onclick="currentSlide(1)"></span>
                <span class="dot" onclick="currentSlide(2)"></span>
                <span class="dot" onclick="currentSlide(3)"></span>
        </div>
    </div>

    <main class="curso">
        <h2>MEUS CURSOS</h2>
        <hr>
        <div class="curso__container-items">
            <section class="curso__item">
                <img src="./img/curso.jpg" alt="Imagem Curso">
                <div class="curso__bloco-mensagem">
                    <h3>PALLENTESQUE MALESUADA</h3>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                    <button>VER CURSO</button>
                </div>
            </section>
            <section class="curso__item">
                <img src="./img/curso.jpg" alt="Imagem Curso">
                <div class="curso__bloco-mensagem">
                    <h3>PALLENTESQUE MALESUADA</h3>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                    <button>VER CURSO</button>
                </div>
            </section>
            <section class="curso__item">
                <img src="./img/curso.jpg" alt="Imagem Curso">
                <div class="curso__bloco-mensagem">
                    <h3>PALLENTESQUE MALESUADA</h3>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                    <button>VER CURSO</button>
                </div>
            </section>
            <section class="curso__item">
                <img src="./img/curso.jpg" alt="Imagem Curso">
                <div class="curso__bloco-mensagem">
                    <h3>PALLENTESQUE MALESUADA</h3>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                    <button>VER CURSO</button>
                </div>
            </section>
            <section class="curso__item">
                <img src="./img/curso.jpg" alt="Imagem Curso">
                <div class="curso__bloco-mensagem">
                    <h3>PALLENTESQUE MALESUADA</h3>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                    <button>VER CURSO</button>
                </div>
            </section>
            <section class="curso__item">
                <img src="./img/curso.jpg" alt="Imagem Curso">
                <div class="curso__bloco-mensagem">
                    <h3>PALLENTESQUE MALESUADA</h3>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                    <button>VER CURSO</button>
                </div>
            </section>
            <section class="curso__item">
                <img src="./img/curso.jpg" alt="Imagem Curso">
                <div class="curso__bloco-mensagem">
                    <h3>PALLENTESQUE MALESUADA</h3>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                    <button>VER CURSO</button>
                </div>
            </section>
            <section class="curso__item curso__adicionar-curso">
                <a href="#">
                    <img src="./img/adicionar-curso.png" alt="Adicionar Curso">
                    <p>ADICIONAR</p>
                    <p>CURSO</p>
                </a>
            </section>
        </div>
    </main>