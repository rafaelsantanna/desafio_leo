// Inicio carousel
var slideIndex = 1;
showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
  showSlides(slideIndex += n);
}

// Dots controls
function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("carousel__slides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1} 
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none"; 
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block"; 
  dots[slideIndex-1].className += " active";
}
//Fim carousel

// Modal Visitante
  if(!localStorage.getItem('ehPrimeiroAcesso'))
  {
      document.getElementsByClassName('background-modal')[0].style.display = 'flex';
  } else {
    document.getElementById('nome-visitante').innerText = localStorage.getItem('first_name') + ' ' + localStorage.getItem('last_name');
  }

  function subscribe() {
    var first_name = document.getElementById('first_name').value;
    var last_name = document.getElementById('last_name').value;
    var email = document.getElementById('email').value;
    var form = document.getElementById('formVisitante');

    if(first_name != '' && last_name != '' && email != '') {
      localStorage.setItem('ehPrimeiroAcesso', 'false');
      localStorage.setItem('first_name', first_name);
      localStorage.setItem('last_name', last_name);
      localStorage.setItem('email', email);
      form.submit();
    } else {
      alert('Preencha os campos');
    }

  }

  function fecharModal() {
    document.getElementsByClassName('background-modal')[0].style.display = 'none';
  }

  // Fim Modal Visitante